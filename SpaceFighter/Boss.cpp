#include "Boss.h"

Boss::Boss()
{
	SetSpeed(100);
	SetMaxHitPoints(40);
	SetCollisionRadius(100);
}

Boss::~Boss()
{

}

void Boss::Update(const GameTime *pGameTime)
{
	/*if (IsActive())
	{
		float y = 0; 
		if (GetPosition().Y < 200)
		{
			y = GetSpeed() * pGameTime->GetTimeElapsed(); 
		}

		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, y);

		if (!IsOnScreen()) Deactivate();

	}
		EnemyShip::Update(pGameTime);*/
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

void Boss::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture2, GetPosition(), Color::White, m_pTexture2->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}