
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	// this checks for any delay in respawning the enemy ship that has been inherited from the ship class
	if (m_delaySeconds > 0)
	{
		// this checks the time in the game, and sets the delay on respawn based on the current time in the game
		m_delaySeconds -= pGameTime->GetTimeElapsed();
		// this then checks if the delay has been executed, if the delay has been executed and is over, a new enemy ship then spawns in
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}
	// checks to see if there is already an active enemy ship
	if (IsActive())
	{
		// puts elapsed game time into the activation variable below
		m_activationSeconds += pGameTime->GetTimeElapsed();
		// this then removes enemies if it flys off the screen or has been alive for more than 2 seconds
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}
		// this then runs the Update() statement which is used for firing weapons
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}