

#include "Level01.h"
#include "BioEnemyShip.h"
#include "Boss.h"




void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture *pTexture2 = pResourceManager->Load<Texture>("Textures\\space-invaders.png");


	const int COUNT = 21;
	

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 5.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	const int BOSS = 1;

	double xPositionsBoss[BOSS] =
	{
		0.5
	};

	double delaysBoss[BOSS] =
	{
		0.5
	};

	
	Vector2 positionBoss; 

	for (int i = 0; i < BOSS; i++)
	{
		delay += delaysBoss[i];
		positionBoss.Set(xPositionsBoss[i] * Game::GetScreenWidth(), -pTexture2->GetCenter().Y);
		Boss *pEnemy2 = new Boss(); 
		pEnemy2->SetTexture(pTexture2);
		pEnemy2->SetCurrentLevel(this);
		pEnemy2->Initialize(positionBoss, (float)delay);
		AddGameObject(pEnemy2);
	}



	Level::LoadContent(pResourceManager);
}

