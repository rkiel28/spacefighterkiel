
#include "Ship.h"


Ship::Ship()
{
	SetPosition(0, 0);
	SetCollisionRadius(10);

	m_speed = 3000;
	m_maxHitPoints = 3;
	m_isInvulnurable = false;

	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage)
{
	// this checks to see whether or not the ship is invulnerable or can take damage from enemies
	if (!m_isInvulnurable)
	{
		// this sets the amount of damage that the ship will take when hit
		m_hitPoints -= damage;
		// after damage has been taken this checks to see if the ship still has any remaining hit points, if not the ship is deactivated/destroyed
		if (m_hitPoints <= 0)
		{
			GameObject::Deactivate();
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

void Ship::FireWeapons(TriggerType type)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}